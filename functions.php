<?php

namespace Lacandona\Theme;

// Make sure we don't expose any info if called directly
if ( ! function_exists( 'add_action' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die;
}

define( 'LACANDONA_DIR', get_stylesheet_directory() . DIRECTORY_SEPARATOR );
define( 'LACANDONA_FUN', LACANDONA_DIR . 'fun' . DIRECTORY_SEPARATOR );
define( 'LACANDONA_URI', get_stylesheet_directory_uri() . '/' );


if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
	include_once( LACANDONA_FUN . 'debug.php' );
}
require_once( LACANDONA_FUN . 'Actions.php' );
new Actions();
