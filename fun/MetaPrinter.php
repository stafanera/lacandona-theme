<?php


namespace Lacandona\Theme;

use Laconst;

class MetaPrinter {

	public static function price()
	{
		$fmt = '<span><i class="fa fa-euro-sign"></i> <span class="byline">%.2f</span></span>';
		$price = get_post_meta(get_the_ID(), Laconst::PRICE, true);

		printf($fmt, $price);
	}

	public static function weight()
	{
		$fmt = '<span><i class="fa fa-box"></i> <span class="byline">%s</span></span>';
		$price = get_post_meta(get_the_ID(), Laconst::WEIGHT, true);

		printf($fmt, $price);
	}

	public static function origin()
	{
		$fmt = '<span><i class="fa fa-map-marker"></i>%s</span>';
		$orig_fmt = ' <span class="byline"><a class="url fn n" href="%s">%s</a></span>';
		$content = [];
		$origins = get_the_terms(get_the_ID(), Laconst::ORIGIN);
		if(! is_array($origins)) {
			return;
		}
		foreach($origins as $origin) {
			$name = $origin->name;
			$url = get_term_link($origin, Laconst::ORIGIN);
			$content[] = sprintf($orig_fmt, $url, $name);
		}
		if(! empty($content)) {
			printf($fmt, join(', ', $content));
		}
	}

	public static function producer()
	{
		global $wpdb;
		$fmt = '<span><i class="fa fa-link"></i>%s</span>';
		$prod_fmt = ' <span class="byline"><a class="url fn n" href="%s" target="%s">%s</a></span>';
		$content = [];

		$ids = get_post_meta(get_the_ID(), Laconst::LINKID);
		$str_ids = implode(',', $ids);

		$producers = $wpdb->get_results("SELECT link_name,link_url,link_target FROM {$wpdb->prefix}links WHERE link_id IN ({$str_ids})", OBJECT);
		if(! is_array($producers)) {
			return;
		}
		foreach($producers as $producer) {
			$content[] = sprintf($prod_fmt, $producer->link_url, $producer->link_target, $producer->link_name);
		}
		if(! empty($content)) {
			printf($fmt, implode(', ', $content));
		}
	}
}