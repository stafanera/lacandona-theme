<?php


namespace Lacandona\Theme;


class Resources {

	public static function load() {
		// sparkling
		wp_enqueue_style( 'sparkling-style', get_template_directory_uri() . '/style.css', null, wp_get_theme()->parent()->get( 'Version' ), 'all');
	}
}