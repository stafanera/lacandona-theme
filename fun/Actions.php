<?php


namespace Lacandona\Theme;


class Actions {

	function __construct() {
		$this->require();
		$this->act();
	}

	private function require() {
		require_once(LACANDONA_FUN . 'MetaPrinter.php');
		require_once(LACANDONA_FUN . 'Resources.php');
	}

	private function act() {
		add_action('wp_enqueue_scripts', [Resources::class, 'load']);
	}
}